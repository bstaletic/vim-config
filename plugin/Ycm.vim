if exists( 'g:ycm_config' )
	finish
endif

let g:ycm_config = 1

" YCM settings
let g:ycm_enable_semantic_highlighting = 0
let g:ycm_enable_inlay_hints = 1
let g:ycm_collect_identifiers_from_comments_and_strings = 1
let g:ycm_complete_in_comments = 1
let g:ycm_seed_identifiers_with_syntax = 1
let g:ycm_always_populate_location_list = 1
let g:ycm_open_loclist_on_ycm_diags = 1
let g:ycm_add_preview_to_completeopt = 1
let g:ycm_goto_buffer_command = 'same-buffer'
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_update_diagnostics_in_insert_mode = 0
let g:ycm_auto_hover = ''
let g:ycm_log_level = 'debug'
let g:ycm_use_clangd = 1
"let g:ycm_clangd_binary_path = 'clangd'
let g:ycm_language_server = []

nmap <silent> yws <Plug>(YCMFindSymbolInWorkspace)
nmap <silent> yds <Plug>(YCMFindSymbolInDocument)
nmap <silent> yih <Plug>(YCMToggleInlayHints)
imap <silent> <C-z> <Plug>(YCMToggleSignatureHelp)
